import React, { Component } from "react";
import {ConnectedProductBottom} from "../components/ProductBottom";
import ProductContainer from "../components/ProductContainer";
import ProductNavbar from "../components/ProductNavbar";
import "./ProductPage.css";
import { connect } from "react-redux";
import * as action from '../actions'
import Loader from "../components/Loader";

export class ProductPage extends Component {
  constructor(props) {
    super(props);

    this.state = {
      
      tempDataToSend: {}
    };
  }

  componentDidMount(){
    const id= this.props.match.params.id;
    
    const myData= this.props.gotData.filter((item)=> item.id===parseInt(id)).pop();
    this.props.addHotelData(myData);
    
  }

  render() {
    
    return (
      (this.props.gotData===undefined)?
      <Loader />
      :
      <>
        <div className="container-fluid sticky-top m-0 p-0">
          <ProductNavbar />
        </div>
        <ProductContainer />
        <ConnectedProductBottom />
      </>
    );
  }
}

const mapStateToProps=(state)=>{
  return{
  gotData: state.apiData.gotData
  }
}

const mapDispatchToProps=(dispatch)=>{
  return{
    addHotelData : (payloadData)=>dispatch(action.fetchHotelById(payloadData))
  }
}

export const ConnectedProductPage=connect(mapStateToProps,mapDispatchToProps)(ProductPage);
