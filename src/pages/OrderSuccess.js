import React from "react";
import { connect } from "react-redux";
import ProductNavbar from "../components/ProductNavbar";
import Loader from "../components/Loader";

function OrderSuccess({ savedUser, orderData }) {
  return orderData === undefined ? (
    <Loader />
  ) : (
    <>
      <ProductNavbar />
      <div className="container-fluid">
        <div className="container d-flex flex-column justify-content-around mt-4">
          <h4 className="text-secondary">
            {" "}
            Hi <strong className="text-dark">{savedUser}</strong> .
          </h4>
          <p className="text-success">Your room is booked successfully.</p>

          <hr></hr>
          <h5 className="pb-2"> Below are your order details.</h5>

          <div className="d-flex flex-column flex-md-row border rounded p-2">
            <div className="container col-lg-4 col-md-6 col-sm-10 pt-2 pb-2">
              <img
                src={orderData.hotelImage}
                alt="hotel"
                className="img-fluid"
              />
            </div>
            <div className="container col-12 d-flex flex-column justify-content-center">
              <h6 className="mt-1">{orderData.hotelName}</h6>
              <h6 className="mt-1">{orderData.hotelLocation}</h6>
              <h6 className="mt-1">
                {orderData.startDate} to {orderData.endDate}
              </h6>
              <h6 className="mt-1">{orderData.adults} Adults</h6>
              <h6 className="mt-1">{orderData.rooms} Rooms</h6>
              <h6 className="mt-1">
                Total charged: Rs <strong>{orderData.price.toLocaleString('en-IN')}</strong>
              </h6>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

const mapStateToProps = (state) => {
  return {
    savedUser: state.apiData.savedUser,
    orderData: state.apiData.orderData,
  };
};

export const ConnectedOrderSuccess = connect(
  mapStateToProps,
  null
)(OrderSuccess);
