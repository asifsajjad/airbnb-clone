import React, { Component } from "react";
import ProductNavbar from "../components/ProductNavbar";
import { Link } from "react-router-dom";
import validator from "validator";
import * as action from "../actions";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";

export class SignupPage extends Component {
  constructor(props) {
    super(props);

    this.state = {
      firstName: "",
      lastName: "",
      email: "",
      password: "",
      repeatPassword: "",
      firstNameError: false,
      lastNameError: false,
      mailError: false,
      passError: false,
      signupSuccess: false,
    };
  }

  addName = (event, fieldName) => {
    const myData = event.target.value;
    if (validator.isAlpha(myData)) {
      this.setState({
        [fieldName]: myData,
        [`${fieldName}Error`]: false,
      });
    } else {
      this.setState({
        [fieldName]: myData,
        [`${fieldName}Error`]: true,
      });
    }
  };

  addPassword = (event) => {
    const myPassword = event.target.value;
    let pError = false;
    if (!validator.isStrongPassword(myPassword)) {
      pError = true;
    }
    this.setState({
      password: myPassword,
      passError: pError,
    });
  };

  addRepeatPassword = (event) => {
    const myRepeatPassword = event.target.value;
    this.setState({
      repeatPassword: myRepeatPassword,
    });
  };

  addMail = (event) => {
    const myMail = event.target.value;
    let myError = false;
    if (!validator.isEmail(myMail)) {
      myError = true;
    }
    this.setState({
      email: myMail,
      mailError: myError,
    });
  };

  checkEmptyFields = () => {
    const firstName = this.state.firstName;
    const lastName = this.state.lastName;
    const email = this.state.email;
    const password = this.state.password;
    if (
      firstName !== "" &&
      lastName !== "" &&
      email !== "" &&
      password !== ""
    ) {
      return false;
    } else {
      return true;
    }
  };

  checkErrors = () => {
    return (
      this.state.firstNameError ||
      this.state.lastNameError ||
      this.state.passError ||
      this.state.mailError
    );
  };

  submitForm = () => {
    console.log("submittriggered");
    console.log(this.checkEmptyFields());
    console.log(this.checkErrors());
    if (
      !this.checkEmptyFields() &&
      !this.checkErrors() &&
      this.state.password === this.state.repeatPassword
    ) {
      this.setState({
        signupSuccess: true,
      });
      const fullName = this.state.firstName + " " + this.state.lastName;
      this.props.addSignupData({
        name: fullName,
        email: this.state.email,
        password: this.state.password,
      });
      console.log("pass");
    } else {
      this.setState({
        signupSuccess: false,
      });
    }
  };

  render() {
    let redirect = "/";
    if (this.props.checkoutRedirect) {
      redirect = "/checkout";
    }

    return (
      <>
        <ProductNavbar />
        <div className="container-fluid">
          <div className="container col-lg-7 col-md-10 col-sm-11 d-flex flex-column rounded mt-4 bg-danger p-2 ">
            <Link to="/">
              {" "}
              <div className="d-flex">
                <i className="fa-solid fa-xmark fa-2x m-2"></i>
              </div>
            </Link>
            <div className="d-flex flex-column bg-white rounded col-lg-5 col-md-5 col-sm-11 mt-4 mb-4 align-self-center">
              <label htmlFor="fName" className="text-dark mt-4">
                First name:
              </label>
              <input
                type="text"
                className="form-control rounded border border-secondary"
                value={this.state.firstName}
                placeholder="first name?"
                id="fName"
                onChange={(e) => this.addName(e, "firstName")}
              />
              {this.state.firstNameError ? (
                <small className="text-danger">Name should be characters</small>
              ) : null}
              <label htmlFor="lName" className="text-dark mt-2">
                Last name:
              </label>
              <input
                type="text"
                className="form-control rounded border border-secondary"
                value={this.state.lastName}
                placeholder="first name?"
                id="lName"
                onChange={(e) => this.addName(e, "lastName")}
              />
              {this.state.lastNameError ? (
                <small className="text-danger">Name should be characters</small>
              ) : null}
              <label htmlFor="email" className="text-dark mt-2">
                E-mail:
              </label>
              <input
                type="text"
                className="form-control rounded border border-secondary"
                value={this.state.email}
                placeholder="e-mail?"
                id="email"
                onChange={(e) => this.addMail(e)}
              />
              {this.state.mailError ? (
                <small className="text-danger">Not an valid e-mail</small>
              ) : null}
              <label htmlFor="password" className="text-dark mt-2">
                Enter a new password:
              </label>
              <input
                type="password"
                className="form-control rounded border border-secondary"
                value={this.state.password}
                placeholder="Enter new password?"
                id="password"
                onChange={(e) => this.addPassword(e)}
              />
              {this.state.passError ? (
                <small className="text-danger">
                  {" "}
                  Password not strong enough{" "}
                </small>
              ) : null}
              <label htmlFor="repeatPassword" className="text-dark mt-2">
                Repeat enter password:
              </label>
              <input
                type="password"
                className="form-control rounded border border-secondary"
                value={this.state.repeatPassword}
                placeholder="Enter password again?"
                id="repeatPassword"
                onChange={(e) => this.addRepeatPassword(e)}
              />
              {this.state.repeatPassword !== this.state.password ? (
                <small className="text-info"> Password not matching </small>
              ) : null}
              <hr></hr>
              <button
                className="btn col-4 btn-danger mt-1 mb-3 rounded"
                onClick={(e) => this.submitForm(e)}
              >
                {" "}
                Sign-up
              </button>
              {this.state.signupSuccess ? (
                <div className="text-success">
                  Signup sucessful!!! <br />
                  <Link to={redirect}>
                    <div className="btn bg-warning mt-1 mb-2">Go back</div>
                  </Link>
                </div>
              ) : null}
            </div>
          </div>
        </div>
        {this.state.signupSuccess ? (
          this.props.checkoutRedirect ? (
            <Redirect to="/checkout" />
          ) : this.props.wishlistRedirect ? (
            <Redirect to="/wishlist" />
          ) : (
            <Redirect to="/" />
          )
        ) : null}
      </>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    addSignupData: (inputData) => dispatch(action.addSignupData(inputData)),
  };
};
const mapStateToProps = (state) => {
  return {
    checkoutRedirect: state.apiData.checkoutRedirect,
    wishlistRedirect: state.apiData.wishlistRedirect,
  };
};

export const ConnectedSignupPage = connect(
  mapStateToProps,
  mapDispatchToProps
)(SignupPage);
