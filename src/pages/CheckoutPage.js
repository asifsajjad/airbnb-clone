import React, { Component } from "react";
import ProductNavbar from "../components/ProductNavbar";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { connect } from "react-redux";
import * as action from "../actions";
import { Link } from "react-router-dom";

export class CheckoutPage extends Component {
  constructor(props) {
    super(props);

    this.state = {
      fromDate: new Date(),
      startDate: new Date().toDateString(),
      toDate: new Date(),
      endDate: new Date().toDateString(),
      loggedIn: false,
      startDateError: false,
      endDateError: false,
      days: 0,
      rooms: "",
      adults: "",
      roomsError: false,
      adultsError: false,
      postError: false,
      orderSuccess: false,
      roomsLessError: false,
    };
  }

  componentDidMount() {
    const today = new Date();
    let tomorrow = new Date();
    tomorrow.setDate(tomorrow.getDate() + this.props.days);
    if (this.props.days !== undefined) {
      if (this.props.savedUser) {
        if (this.props.tempBookingData) {
         
          this.setState({
            days: this.props.days,
            rooms: this.props.rooms,
            adults: this.props.adults,
            loggedIn: true,
            fromDate: new Date(this.props.tempBookingData.startDate),
            startDate: new Date(this.props.tempBookingData.startDate).toDateString(),
            toDate: new Date(this.props.tempBookingData.endDate),
            endDate: new Date(this.props.tempBookingData.endDate).toDateString(),
          });
        } else {
          this.setState({
            days: this.props.days,
            rooms: this.props.rooms,
            adults: this.props.adults,
            loggedIn: true,
            fromDate: today,
            startDate: today.toDateString(),
            toDate: tomorrow,
            endDate: tomorrow.toDateString(),
          });
        }

        this.props.redirectToLogin(false);
      } else {
        this.setState({
          days: this.props.days,
          rooms: this.props.rooms,
          adults: this.props.adults,
          fromDate: today,
          startDate: today.toDateString(),
          toDate: tomorrow,
          endDate: tomorrow.toDateString(),
        });
        // this.props.redirectToLogin(true);
      }
    }
  }
  setStartDate = (gotDate) => {
    const startDays = this.getDateDiff(new Date(), gotDate);
    const days = this.getDateDiff(gotDate, this.state.toDate) - 1;
    let pushDays = days;
    if (days < 1) {
      pushDays = 1;
    }
    let tomorrow = new Date();
    tomorrow.setDate(gotDate.getDate() + 1);
    if (startDays >= 0) {
      if (days < 1) {
        this.setState({
          fromDate: gotDate,
          startDate: gotDate.toDateString(),
          toDate: tomorrow,
          endDate: tomorrow.toDateString(),
          startDateError: false,
          days: pushDays,
        });
      } else {
        this.setState({
          fromDate: gotDate,
          startDate: gotDate.toDateString(),
          startDateError: false,
          days: pushDays,
        });
      }
    }
    //  else {
    //   this.setState({
    //     startDateError: true,
    //   });
    // }
  };

  getDateDiff = (fromDate, toDate) => {
    const milisecInDay = 1000 * 60 * 60 * 24;
    const diffInTime = toDate - fromDate;
    return Math.ceil(diffInTime / milisecInDay);
  };

  setEndDate = (gotDate) => {
    const days = this.getDateDiff(this.state.fromDate, gotDate) - 1;
    if (days > 0) {
      this.setState({
        toDate: gotDate,
        endDate: gotDate.toDateString(),
        days: days,
        endDateError: false,
      });
    } else {
      this.setState({
        endDateError: true,
        days: days,
        toDate: gotDate,
        endDate: gotDate.toDateString(),
      });
    }
  };

  setAdults = (event) => {
    const adultsData = event.target.value;
    let adultsError = false;
    if (adultsData < 1 || parseFloat(adultsData) !== parseInt(adultsData)) {
      adultsError = true;
    }
    if (this.state.rooms > adultsData / 2) {
      this.setState({
        adults: adultsData,
        adultsError,
      });
    } else {
      this.setState({
        adults: adultsData,
        adultsError,
        rooms: Math.ceil(adultsData / 2),
      });
    }
  };

  setRooms = (event) => {
    const roomsData = event.target.value;
    let roomsError = false;
    let roomsLessError = false;
    if (roomsData < 1 || parseFloat(roomsData) !== parseInt(roomsData)) {
      roomsError = true;
    }
    if (roomsData < Math.ceil(this.state.adults / 2)) {
      roomsLessError = true;
    }
    this.setState({
      rooms: roomsData,
      roomsError,
      roomsLessError,
    });
  };

  hasPostError = () => {
    const startDateError = this.state.startDateError;
    const endDateError = this.state.endDateError;
    const roomsError = this.state.roomsError;
    const adultsError = this.state.adultsError;
    if (startDateError || endDateError || roomsError || adultsError) {
      return true;
    } else return false;
  };

  addOrderDetailsToStore = () => {
    const startDateToPost = this.state.startDate;
    const endDateToPost = this.state.endDate;
    const adults = this.state.adults;
    const rooms = this.state.rooms;
    const hotelName = this.props.hotelData.title;
    const hotelId = this.props.hotelData.id;
    const price =
      this.props.hotelData.price * this.state.rooms * this.state.days;

    if (this.hasPostError()) {
      console.log("post error");
      this.setState({
        postError: true,
        orderSuccess: false,
      });
    } else {
      this.setState({
        postError: false,
        orderSuccess: true,
      });
      this.props.addOrderData({
        hotelName,
        hotelId,
        hotelImage: this.props.hotelData.images[0],
        hotelLocation: this.props.hotelData.location,
        startDate: startDateToPost,
        endDate: endDateToPost,
        adults,
        rooms,
        price,
      });
    }
  };

  sendInfoToStoreBeforeRedirection = () => {
    this.props.redirectToLogin(true);
    this.props.saveBookingData({
      days: this.state.days,
      adults: this.state.adults,
      rooms: this.state.rooms,
    });
    this.props.storeTempBookingInfo({
      startDate: this.state.fromDate.toString(),
      endDate: this.state.toDate.toString(),
    });
  };

  render() {
    const finalPrice =
      this.props.hotelData.price * this.state.rooms * this.state.days;
    const displayPrice = finalPrice.toLocaleString("en-IN");
    return (
      <>
        <ProductNavbar />
        <div className="container-fluid p-4 d-flex flex-column">
          <div className="container col-lg-10 col-12 ">
            <h3>Confirm and Pay</h3>
          </div>
          <div className="container col-12 col-lg-10 d-flex flex-column  flex-md-row flex-lg-row mt-1">
            <div className=" col-lg-6 col-md-6 col-sm-12 d-flex flex-column mt-3 rounded p-2">
              {/* Left side */}
              <h3>Your trip</h3>

              <div className="col-12 d-flex flex-column mt-3 mb-2 justify-content-start p-0">
                <h4>Dates</h4>

                <div className="d-flex flex-column p-1 m-1">
                  <h5>
                    From: <strong>{this.state.startDate}</strong>
                  </h5>
                  <DatePicker
                    selected={this.state.fromDate}
                    onChange={(date) => this.setStartDate(date)}
                  />
                  {this.state.startDateError ? (
                    <small className="text-danger m-0">
                      {" "}
                      Please select date in future.
                    </small>
                  ) : null}
                </div>

                <div className="d-flex flex-column p-1 m-1 mb-0">
                  <h5>
                    To: <strong>{this.state.endDate}</strong>
                  </h5>

                  <DatePicker
                    selected={this.state.toDate}
                    onChange={(date) => this.setEndDate(date)}
                  />

                  {this.state.endDateError ? (
                    <small className="text-danger m-0">
                      {" "}
                      End date should be ahead of start date.
                    </small>
                  ) : null}
                </div>

                <div className="p-1 ml-2">
                  {" "}
                  You have selected {this.state.days} days.
                </div>
              </div>
              {/* Left side end */}
            </div>
            <div className="col-lg-6 col-md-6 col-sm-12 d-flex flex-column mt-3 rounded p-0">
              {/* Right Side */}
              <div className="container col-12 d-flex flex-column align-items-start pt-3">
                <div className="d-flex flex-row">
                  <div className="container col-4">
                    <img
                      src={this.props.hotelData.images[0]}
                      alt="hotel"
                      className="img-fluid"
                    />
                  </div>
                  <div className="container col-8 d-flex align-items-center">
                    <h6>{this.props.hotelData.title}</h6>
                  </div>
                </div>
                <div className="d-flex align-items-center  mt-3 p-3">
                  {" "}
                  <input
                    type="number"
                    step="1"
                    min="1"
                    className="form-control-sm col-3 border border-secondary mr-3"
                    value={this.state.adults}
                    onChange={this.setAdults}
                  />
                  <h5 className="mr-4"> Guests/Adults </h5>
                </div>
                {this.state.adultsError ? (
                  <small className="text-danger p-0 m-0">
                    Please enter positive integer in adults
                  </small>
                ) : null}
                <div className="d-flex align-items-center mt-3 p-3">
                  {" "}
                  <input
                    type="number"
                    step="1"
                    min="1"
                    className="form-control-sm col-3 border border-secondary mr-3"
                    value={this.state.rooms}
                    onChange={this.setRooms}
                  />
                  <h5 className="mr-4"> Room(s) </h5>
                </div>
                {this.state.roomsError ? (
                  <small className="text-danger p-0 m-0">
                    Please enter positive integer in rooms
                  </small>
                ) : this.state.roomsLessError ? (
                  <small className="text-danger">
                    {" "}
                    Only 2 guests are allowed in a room.
                  </small>
                ) : null}
                <hr></hr>
                <div className="d-flex align-items-center justify-content-end m-3">
                  {" "}
                  <h5 className="mr-4"> Total Price: </h5>
                  <h5>
                    Rs <strong>{displayPrice}</strong>
                  </h5>
                </div>
              </div>
              {/* Right side end */}
            </div>
          </div>
          <div className=" col-lg-9 col-md-11 col-sm-12  align-self-center m-4">
            {this.state.loggedIn ? (
              <div>
                <h5> Please confirm email-id and proceed to book.</h5>
                <hr></hr>
                <p>
                  E-mail :<strong> {this.props.savedUser}</strong>
                </p>
                <hr></hr>
                {this.state.startDateError ||
                this.state.endDateError ||
                this.state.roomsError ||
                this.state.adultsError ||
                this.state.roomsLessError ? (
                  <p className="text-danger">
                    Please Enter the booking data correctly to proceed
                  </p>
                ) : (
                  <Link to="/myorder">
                    <div
                      className="btn btn-danger col-12"
                      onClick={this.addOrderDetailsToStore}
                    >
                      {" "}
                      Pay and Book Now{" "}
                    </div>
                  </Link>
                )}
                {this.state.postError ? (
                  <small className="text-danger">
                    OOPS! Looks like you have entered incompatible data above.
                  </small>
                ) : null}
                <div className="col-12">
                  <small>
                    {" "}
                    By clicking on the book button you are automatically
                    agreeing to our terms and conditions.
                  </small>
                </div>
              </div>
            ) : (
              <div className="col-12 col-md-8 p-0">
                <h5> Please sign-in/register to complete the booking.</h5>
                {this.state.startDateError ||
                this.state.endDateError ||
                this.state.roomsError ||
                this.state.adultsError  ||
                this.state.roomsLessError? (
                  <p className="text-danger">
                    Please Enter the booking data correctly to proceed
                  </p>
                ) : (
                  <Link to="/login">
                    {" "}
                    <button
                      className="form-control col-5 btn btn-danger mt-2"
                      onClick={this.sendInfoToStoreBeforeRedirection}
                    >
                      {" "}
                      Sign-in{" "}
                    </button>
                  </Link>
                )}
              </div>
            )}
          </div>
        </div>
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    hotelData: state.apiData.selectedHotel,
    days: state.apiData.days,
    adults: state.apiData.adults,
    rooms: state.apiData.rooms,
    savedUser: state.apiData.savedUser,
    tempBookingData: state.apiData.tempBookingData,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    addOrderData: (myOrder) => dispatch(action.addOrderData(myOrder)),
    redirectToLogin: (boolData) => dispatch(action.redirectToLogin(boolData)),
    saveBookingData: (userPayload) =>
      dispatch(action.saveBookingData(userPayload)),
    storeTempBookingInfo: (myPayload) =>
      dispatch(action.storeTempBookingInfo(myPayload)),
  };
};

export const ConnectedCheckoutPage = connect(
  mapStateToProps,
  mapDispatchToProps
)(CheckoutPage);
