import React, { Component } from "react";
import ProductNavbar from "../components/ProductNavbar";
import { Link } from "react-router-dom";
import validator from "validator";
import * as action from "../actions";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";
import axios from "axios";

export class LoginPage extends Component {
  constructor(props) {
    super(props);

    this.state = {
      inputNumber: "",
      inputPassword: "",
      numberError: false,
      passwordError: false,
      loggedIn: false,
      loginSuccess: false,
      loginFailed: false,
    };
  }

  componentDidMount() {
    if (this.props.savedUser) {
      this.setState({ loggedIn: true });
    }
  }

  enterNumber = (event) => {
    const enteredNumber = event.target.value;
    if (!validator.isEmail(enteredNumber)) {
      this.setState({ inputNumber: enteredNumber, numberError: true });
    } else {
      this.setState({ inputNumber: enteredNumber, numberError: false });
    }
  };

  enterPassword = (event) => {
    const enteredPassword = event.target.value;
    this.setState({ inputPassword: enteredPassword });
  };

  validateUser = (userArray, userEmail, userPassword) => {
    const user = userArray.filter((item) => item.email === userEmail).pop();
    let loginSuccess = false;
    let loginFailed = false;
    if (user) {
      if (user.password === userPassword) {
        this.props.saveUserData(this.state.inputNumber);
        this.props.addSignupData(user);
        loginSuccess = true;
      } else {
        loginFailed = true;
      }
    } else {
      loginFailed = true;
    }
    this.setState({
      loginSuccess,
      loginFailed,
    });
  };

  submitForm = (event) => {
    event.preventDefault();
    let perror = false,
      nerror = false;

    if (!validator.isStrongPassword(this.state.inputPassword)) {
      perror = true;
    }
    if (!validator.isEmail(this.state.inputNumber)) {
      nerror = true;
    }
    if (
      validator.isStrongPassword(this.state.inputPassword) &&
      validator.isEmail(this.state.inputNumber)
    ) {
      perror = false;
      nerror = false;
      axios("./userData.json")
        .then((response) => {
          this.validateUser(
            response.data,
            this.state.inputNumber,
            this.state.inputPassword
          );
        })
        .catch(console.log);
    }
    this.setState({
      numberError: nerror,
      passwordError: perror,
    });
  };

  setSignout=()=>{
    this.setState({
      loggedIn:false
    });
    this.props.saveUserData({});
    this.props.addSignupData({});
  }

  render() {
    let redirect = "/";
    if (this.props.checkoutRedirect) {
      redirect = "/checkout";
    }
    return (
      <>
        <ProductNavbar />
        <div className="container-fluid">
          <div className="container col-lg-7 col-md-10 col-sm-11 d-flex flex-column rounded mt-4 bg-danger p-2 ">
            <Link to="/">
              {" "}
              <div className="d-flex">
                <i className="fa-solid fa-xmark fa-2x m-2"></i>
              </div>
            </Link>
            {this.state.loggedIn ? (
              <div className="container col-lg-5 col-md-6 col-sm-10 text-success bg-white border border-dark rounded text-center">
                <i className="fa-solid fa-user-check fa-3x m-4"></i>
                <h3>User already logged In!</h3>
                <Link to="/"> <button className="btn btn-danger border rounded m-3" onClick={this.setSignout}> Sign-out</button></Link>
              </div>
            ) : (
              <div className="d-flex flex-column bg-white rounded col-lg-5 col-md-5 col-sm-11 mt-4 mb-4 align-self-center">
                <label htmlFor="mobile" className="text-dark mt-4">
                  Enter your email-id:
                </label>
                <input
                  type="text"
                  className="mt-1 form-control rounded border border-secondary"
                  value={this.state.inputNumber}
                  placeholder="Enter your mail id?"
                  id="mobile"
                  onChange={(e) => this.enterNumber(e)}
                />
                {this.state.numberError ? (
                  <div className="text-danger">Please enter correct mail.</div>
                ) : null}
                <label htmlFor="password" className="text-dark mt-4">
                  Enter your password:
                </label>
                <input
                  type="password"
                  className="mt-3 form-control rounded border border-secondary"
                  value={this.state.inputPassword}
                  placeholder="Enter your password?"
                  id="password"
                  onChange={(e) => this.enterPassword(e)}
                />
                {this.state.passwordError ? (
                  <div className="text-danger">
                    {" "}
                    Please check your password{" "}
                  </div>
                ) : null}
                <hr></hr>
                <button
                  className="btn col-4 btn-danger mt-3 rounded"
                  onClick={(e) => this.submitForm(e)}
                >
                  {" "}
                  Login
                </button>
                {this.state.loginSuccess ? (
                  <div className="text-success">
                    Login sucessful!!! <br />
                    <Link to={redirect}>
                      <div className="btn bg-warning mt-1 mb-2">Go back</div>
                    </Link>
                  </div>
                ) : this.state.loginFailed ? (
                  <div className="text-danger">
                    Username / password doesn't match <br />
                  </div>
                ) : null}
                <small className="mt-3 mb-5">
                  {" "}
                  Not registered yet?{" "}
                  <Link to="/signup">
                    <strong>Signup</strong>
                  </Link>
                </small>
              </div>
            )}
          </div>
        </div>
        {this.state.loginSuccess ? (
          this.props.checkoutRedirect ? (
            <Redirect to="/checkout" />
          ) : this.props.wishlistRedirect ? (
            <Redirect to="/wishlist" />
          ) : (
            <Redirect to="/" />
          )
        ) : null}
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    savedUser: state.apiData.savedUser,
    checkoutRedirect: state.apiData.checkoutRedirect,
    wishlistRedirect: state.apiData.wishlistRedirect,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    saveUserData: (userPayload) => dispatch(action.saveUserLogin(userPayload)),
    addSignupData: (inputData) => dispatch(action.addSignupData(inputData)),
  };
};

export const ConnectedLoginPage = connect(
  mapStateToProps,
  mapDispatchToProps
)(LoginPage);
