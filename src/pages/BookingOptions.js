import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import * as action from "../actions";
import { Redirect } from "react-router-dom";
import ProductNavbar from "../components/ProductNavbar";

export class BookingOptions extends Component {
  constructor(props) {
    super(props);

    this.state = {
      days: 1,
      daysError: false,
      adults: "",
      adultsError: false,
      rooms: "",
      roomserror: false,
      roomsLessError: false,
      submitError: false,
      success: false,
    };
  }

  enterDays = (event) => {
    let numDays = event.target.value;

    if (isNaN(numDays * 1) || parseInt(numDays) <= 0 || numDays==="") {
      this.setState({ daysError: true, days: numDays });
    } else {
      if (isNaN(parseInt(numDays))) {
        numDays = "";
      } else {
        numDays = parseInt(numDays);
      }
      this.setState({ daysError: false, days: numDays });
    }
  };

  enterAdults = (event) => {
    let adultsData = event.target.value;
    let adultsError = false;
    if (adultsData < 1 || parseFloat(adultsData) !== parseInt(adultsData)) {
      adultsError = true;
    }
    if (this.state.rooms > adultsData / 2) {
      if (isNaN(parseInt(adultsData))) {
        adultsData = "";
      } else {
        adultsData = parseInt(adultsData);
      }
      this.setState({
        adults: adultsData,
        adultsError,
      });
    } else {
      this.setState({
        adults: adultsData,
        adultsError,
        rooms: Math.ceil(adultsData / 2),
      });
    }
  };

  enterRooms = (event) => {
    let numRooms = event.target.value;
    let roomsLessError = true;
    if (isNaN(numRooms * 1) || parseInt(numRooms)<=0) {
      this.setState({ roomsError: true, rooms: numRooms });
    } else {
      if (isNaN(parseInt(numRooms))) {
        numRooms = "";
      } else {
        numRooms = parseInt(numRooms);
      }
      if (numRooms >= Math.ceil(this.state.adults / 2)) {
        roomsLessError = false;
      }
      this.setState({ roomsError: false, rooms: numRooms, roomsLessError });
    }
  };

  submitOptions = (event) => {
    event.preventDefault();
    const gotAdults = this.state.adults;
    const gotRooms = this.state.rooms;
    const gotDays = this.state.days;
    if (
      gotDays > 0 &&
      gotAdults > 0 &&
      gotRooms > 0 &&
      gotRooms >= Math.ceil(gotAdults / 2)
    ) {
      this.props.saveBookingData({
        days: this.state.days,
        adults: this.state.adults,
        rooms: this.state.rooms,
      });
      this.setState({ success: true, submitError: false });
    } else {
      if (gotRooms < Math.ceil(gotAdults / 2)) {
        this.setState({
          submitError: true,
          success: false,
          roomsLessError: true,
        });
      } else {
        this.setState({ submitError: true, success: false });
      }
    }
  };

  render() {
    return this.state.success ? (
      <Redirect to="/" />
    ) : (
      <div className="container-fluid p-0">
        <ProductNavbar />
        <div className="container col-lg-4 col-md-6 col-sm-11 mt-4 bg-danger d-flex flex-column rounded">
          <Link to="/">
            {" "}
            <div className="d-flex">
              <i className="fa-solid fa-xmark fa-2x m-2"></i>
            </div>
          </Link>
          <label htmlFor="days" className="text-white mt-4">
            Please tell us your duration of stay:
          </label>
          <input
            type="number"
            className="mt-1 form-control rounded border border-danger"
            value={this.state.days}
            placeholder="Number of days?"
            id="days"
            onChange={(e) => this.enterDays(e)}
          />
          {this.state.daysError ? (
            <div className="text-dark">
              <small>Please enter days correctly.</small>
            </div>
          ) : null}

          <label htmlFor="adults" className="text-white mt-4">
            Adults:
          </label>
          <input
            type="number"
            className="mt-1 form-control rounded border border-danger"
            value={this.state.adults}
            placeholder="How many adults?"
            id="adults"
            onChange={(e) => this.enterAdults(e)}
          />
          {this.state.adultsError ? (
            <div className="text-dark">
              {" "}
              <small>
                {" "}
                Please enter only positive integer values in adults.
              </small>
            </div>
          ) : null}

          <label htmlFor="rooms" className="text-white mt-4">
            Number of rooms:
          </label>
          <input
            type="number"
            className="mt-1 form-control rounded border border-danger"
            value={this.state.rooms}
            placeholder="How many rooms you need?"
            id="rooms"
            onChange={(e) => this.enterRooms(e)}
          />
          {this.state.roomsError ? (
            <div className="text-dark">
              {" "}
              <small>Please enter only positive integer values in rooms.</small>
            </div>
          ) : this.state.roomsLessError ? (
            <small className="text-dark">
              {" "}
              Only 2 guests are allowed in a room.
            </small>
          ) : null}
          <hr></hr>
          <button
            className="btn btn-white col-4 border-0 mb-4 rounded"
            onClick={(e) => this.submitOptions(e)}
          >
            {" "}
            Proceed{" "}
          </button>
          {this.state.submitError ? (
            <div className="text-warning">
              <br /> OOPS! Please check if you have correctly filled the above
              data.{" "}
            </div>
          ) : null}
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    days: state.apiData.days,
    adults: state.apiData.adults,
    rooms: state.apiData.rooms,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    saveBookingData: (userPayload) =>
      dispatch(action.saveBookingData(userPayload)),
  };
};

export const ConnectedBookingOptions = connect(
  mapStateToProps,
  mapDispatchToProps
)(BookingOptions);
