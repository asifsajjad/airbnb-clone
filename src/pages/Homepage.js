import React, { Component } from "react";
import { connect } from "react-redux";
import BottomBar from "../components/BottomBar";
import Category from "../components/Category";
import {ConnectedMainContainer} from "../components/MainContainer";
import Navbar from "../components/Navbar";
import "./Homepage.css";


export class Homepage extends Component {
  render() {
    return (
      <><div className="sticky-top d-flex width-100vw flex-column">
        <Navbar />
        <Category />
        </div>
        <ConnectedMainContainer />
        <BottomBar />
      </>
    );
  }
}

export const ConnectedHomepage= connect()(Homepage);
