import React, { Component } from "react";
import { connect } from "react-redux";
import Navbar from "../components/Navbar";
import Loader from "../components/Loader";
import { Link } from "react-router-dom";

export class SearchPage extends Component {
  constructor(props) {
    super(props);

    this.state = {
      displaySearchArray: [],
    };
  }
  componentDidMount() {
    if (this.props.dataArray) {
      const firstFiveDataArray = this.props.dataArray.slice(0, 5);
      this.setState({
        displaySearchArray: firstFiveDataArray,
      });
    }
  }
  searchAndDisplay = (event) => {
    const searchText = event.target.value;
    const resultArray = this.props.dataArray.filter((item) =>
      item.location.toLowerCase().includes(searchText.toLowerCase())
    );
    // console.log(resultArray);
    this.setState({
      displaySearchArray: resultArray,
    });
  };

  renderSearchResults = (searchArray) => {
    return searchArray.map((item) => {
      return (
        <div
          className="col-lg-3 col-md-5 col-sm-11 d-flex flex-column p-1 justify-content-center"
          key={item.id}
        >
          <div className="col-12 d-flex flex-column myCards border rounded">
            <Link to={`/${item.id}`}>
              <h5 className="text-dark "> {item.title}</h5>
              <h6 className="text-secondary">{item.location}</h6>
            </Link>
          </div>
        </div>
      );
    });
  };

  render() {
    return this.props.dataArray === undefined ? (
      <Loader />
    ) : (
      <>
        <Navbar />
        <div className="container-fluid">
          <Link to="/">
            {" "}
            <div className="d-flex">
              <i className="fa-solid fa-xmark fa-2x m-2"></i>
            </div>
          </Link>
          {/* Search start */}
          <div className="container col-lg-5 col-md-6 col-sm-11 mb-4">
            <div className="container col-10 w-100 bg-danger p-0 product-search d-flex justify-content-between cursor-pointer mt-4">
              <div className="col-3 d-flex align-items-center justify-content-center">
                <i className="fa-solid fa-magnifying-glass"></i>
              </div>

              <input
                type="text"
                className="form-control border-0 input-shadow  w-75 product-search"
                placeholder="Search"
                aria-label="Search"
                aria-describedby="search-addon"
                onChange={(e) => this.searchAndDisplay(e)}
              />
            </div>
          </div>
          {/* Search end */}
          {this.state.displaySearchArray.length !== 0 ? (
            <div className="container col-lg-10 col-sm-12 d-flex flex-column flex-md-row flex-wrap">
              {this.renderSearchResults(this.state.displaySearchArray)}
            </div>
          ) : (
            <div className="container col-lg-10 col-md-10 col-sm-11 d-flex flex-column mt-4 border">
              <h3 className="text-danger text-center mt-2">
                {" "}
                Unfortunately, we could not find the searched house.
              </h3>
              <h5 className="text-secondary text-center mt-2"> We are constantly adding new properties. Please revisit again. </h5>
            </div>
          )}
        </div>
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    dataArray: state.apiData.gotData,
  };
};

export const ConnectedSearchPage = connect(mapStateToProps, null)(SearchPage);
