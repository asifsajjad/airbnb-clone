import React, { Component } from "react";
import ProductNavbar from "../components/ProductNavbar";
import Loader from "../components/Loader";
import { connect } from "react-redux";
import * as actions from "../actions";
import { Link } from "react-router-dom";
import emptyCart from '../images/empty-cart.png'

export class WishlistPage extends Component {
  constructor(props) {
    super(props);

    this.state = {
      wishlistData: [],
    };
  }

  addWishlistRedirect = (boolInput) => {
    this.props.addWishlistRedirect(boolInput);
  };

  removeWishlistItem = (id) => {
    let wishlistArray = this.props.wishlistData;
    const newWishListArray = wishlistArray.filter((item) => item.id !== id);
    this.props.addNewWishlistArray(newWishListArray);
  };

  renderWishlistTiles = (wishlistArray) => {
    return wishlistArray.map((item) => {
      return (
        <div
          className="col-lg-8 col-md-9 col-sm-11 d-flex flex-column flex-md-row border border-danger rounded mb-4 myCards"
          key={item.id}
        >
          <div
            className="d-flex justify-content-end"
            onClick={() => this.removeWishlistItem(item.id)}
          >
            <i className="fa-solid fa-trash fa m-2 cursor-pointer"></i>
          </div>
          <Link to={`/${item.id}`}>
            <div className="col-lg-6 col-md-8 col-sm-12 p-0 m-0 d-flex flex-column flex-md-row ">
              <div className="col-lg-5 col-md-5 col-sm-12 p-3">
                <img
                  src={item.images[0]}
                  className="img-fluid"
                  alt="wishlist"
                />
              </div>
              <div className="col-md-10 col-sm-12 p-3">
                <h5 className="text-dark">{item.title}</h5>
                <h5 className="text-secondary">{item.location}</h5>
              </div>
            </div>
          </Link>
        </div>
      );
    });
  };

  render() {
    return this.props.wishlistData === undefined ? (
      <Loader />
    ) : (
      <>
        <ProductNavbar />
        <div className="container-fluid">
          {this.props.savedUser ? (
            <div className="container-fluid d-flex flex-column">
              <h3 className="text-secondary text-center m-2"> Wishlist</h3>
              <hr></hr>
              {this.props.wishlistData.length !== 0 ? (
                this.renderWishlistTiles(this.props.wishlistData)
              ) : (
                <div className="col-md-8 col-sm-12 text-danger text-center m-auto">
                    <img src={emptyCart} className="img-fluid col-6 m-3" alt="nocart" />
                  <h2 className="mt-3">
                    It's lonely in here.                    
                  </h2>
                  <h4 className="text-secondary">Please add items to wish list from the main page.</h4>
                </div>
              )}
            </div>
          ) : (
            <div className=" container col-lg-6 col-md-8 col-sm-11 d-flex flex-column border rounded mt-4 justify-content-center align-items-center">
              <h4 className="text-dark text-center mt-4 mb-4">
                Please login to view your wishlist.
              </h4>
              <Link to="/login">
                <button
                  className="btn btn-danger col-12 mb-4"
                  onClick={() => this.addWishlistRedirect(true)}
                >
                  {" "}
                  Login{" "}
                </button>
              </Link>
            </div>
          )}
        </div>
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    wishlistData: state.apiData.wishlistData,
    savedUser: state.apiData.savedUser,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    addNewWishlistArray: (newArray) =>
      dispatch(actions.removeFromWishlist(newArray)),
    addWishlistRedirect: (boolInput) =>
      dispatch(actions.addWishlistRedirect(boolInput)),
  };
};

export const ConnectedWishlistPage = connect(
  mapStateToProps,
  mapDispatchToProps
)(WishlistPage);
