const addToWishlist = (productObj) => {
  return {
    type: "ADDWISHLIST",
    payload: productObj,
  };
};

export default addToWishlist;
