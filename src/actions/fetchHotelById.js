const fetchHotelById= (hotelData)=>{
    return {
        type: 'FETCHID',
        payload: hotelData
    }
}
export default fetchHotelById;