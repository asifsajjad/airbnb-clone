const addSignupData = (inputData) => {
  return {
    type: "SIGNUP",
    payload: inputData,
  };
};

export default addSignupData;
