import fetchAPI from "./fetchAPI";
import fetchHotelById from "./fetchHotelById";
import saveUserLogin from "./saveUserLogin";
import saveBookingData from "./bookingData";
import addSignupData from "./addSignupData";
import addOrderData from "./addOrderData";
import redirectToLogin from "./redirectToLogin";
import addToWishlist from "./addToWishlist";
import removeFromWishlist from "./removeFromWishlist";
import addWishlistRedirect from "./addWishlistRedirect";
import storeTempBookingInfo from "./storeTempBookingInfo";

export {
  fetchAPI,
  fetchHotelById,
  saveUserLogin,
  saveBookingData,
  addSignupData,
  addOrderData,
  redirectToLogin,
  addToWishlist,
  removeFromWishlist,
  addWishlistRedirect,
  storeTempBookingInfo,
};
