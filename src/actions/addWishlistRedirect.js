const addWishlistRedirect=(boolInput)=>{
    return {
        type: 'WISHLISTREDIRECT',
        payload: boolInput
    }
}

export default addWishlistRedirect;