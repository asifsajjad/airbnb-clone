const addOrderData = (inputData) => {
  return {
    type: "ORDER",
    payload: inputData,
  };
};

export default addOrderData;
