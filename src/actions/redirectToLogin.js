const redirectToLogin = (boolData) => {
  return {
    type: "REDIRECT",
    payload: boolData
  };
};

export default redirectToLogin;
