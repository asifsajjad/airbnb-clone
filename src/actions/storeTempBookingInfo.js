const storeTempBookingInfo = (bookingObj) => {
  return {
    type: "TEMPBOOKING",
    payload: bookingObj,
  };
};

export default storeTempBookingInfo;
