const removeFromWishlist = (newWishlistArray)=>{
    return{
        type: "REMOVEWISHLIST",
        payload: newWishlistArray
    }
}

export default removeFromWishlist;