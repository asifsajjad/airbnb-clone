import React, { Component } from "react";
import { connect } from "react-redux";
import { ConnectedHomepage } from "./pages/Homepage";
import { ConnectedProductPage } from "./pages/ProductPage";
import * as action from "./actions";
import axios from "axios";
import { Route, Switch } from "react-router-dom";
import {ConnectedLoginPage} from "./pages/LoginPage";
import { ConnectedBookingOptions } from "./pages/BookingOptions";
import {ConnectedSignupPage} from "./pages/SignupPage";
import {ConnectedCheckoutPage} from "./pages/CheckoutPage";
import { ConnectedOrderSuccess } from "./pages/OrderSuccess";
import { ConnectedWishlistPage } from "./pages/WishlistPage";
import { ConnectedSearchPage } from "./pages/SearchPage";


export class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      initial: 0,
    };
  }

  componentDidMount() {
    axios("./data.json").then((response) => {
      this.props.setData(response.data);
    });
  }

  render() {
    return (
      
      <>
        <Switch>
          
          <Route path="/options" exact render={(props)=><ConnectedBookingOptions {...props} />} />
          <Route path="/login" exact render={(props)=><ConnectedLoginPage {...props} />} />
          <Route path="/signup" exact render={(props)=><ConnectedSignupPage {...props} />} />
          <Route path="/checkout" exact render={(props)=><ConnectedCheckoutPage {...props} />} />
          <Route path="/myorder" exact render={(props)=><ConnectedOrderSuccess {...props} />} />
          <Route path="/wishlist" exact render={(props)=><ConnectedWishlistPage {...props} />} />
          <Route path="/search" exact render={(props)=><ConnectedSearchPage {...props} />} />
          <Route
            path="/:id"
            exact
            render={(props) => <ConnectedProductPage {...props} />}
          />
          <Route
            path="/"
            exact
            render={(props) => <ConnectedHomepage {...props} />}
          />
        </Switch>
      </>
      
    );
  }
}

const mapStateToProps = (state) => {
  return {
    apiData: state.apiData.gotData,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    setData: (inData) => dispatch(action.fetchAPI(inData)),
  };
};

export const ConnectedApp = connect(mapStateToProps, mapDispatchToProps)(App);
