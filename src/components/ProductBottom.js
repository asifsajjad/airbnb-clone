import React from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import Loader from "./Loader";

function ProductBottom({ days, adults, rooms, hotelData }) {
  
  return hotelData === undefined ? (
    <Loader />
  ) : (
    <>
      <div className="container-fluid fixed-bottom d-lg-none d-flex justify-content-around bg-white">
        <div className="d-flex flex-column mt-2">
          <h5>
            {" "}
            Rs <strong>{(hotelData.price * rooms).toLocaleString('en-IN')}</strong> night
          </h5>
          <h6>
            <strong>{rooms}</strong> Rooms and <strong>{adults}</strong> Adults.
          </h6>
        </div>
        <div>
          <Link to="/checkout">
          <button type="button" className="btn btn-danger mt-3">
            {" "}
            Check-Out
          </button>
          </Link>
        </div>
      </div>
    </>
  );
}

const mapStateToProps = (state) => {
  return {
    days: state.apiData.days,
    adults: state.apiData.adults,
    rooms: state.apiData.rooms,
    hotelData: state.apiData.selectedHotel,
  };
};

export const ConnectedProductBottom = connect(
  mapStateToProps,
  null
)(ProductBottom);
