import React from "react";
import { Link } from "react-router-dom";

function BottomBar() {
  return (
    <>
      <nav className="navbar fixed-bottom navbar-expand-lg navbar-light bg-light override-flex navbar-property-bottom">
        <div className="collapse navbar-collapse l-nav">
          <div className="d-flex align-items-center h-100">
            <h6 className="ml-3">&#169; AIRBNB</h6>
            <h6 className="ml-3 cursor-pointer">
              <a href="https://www.airbnb.co.in/help/article/2855/airbnb-privacy">
                PRIVACY
              </a>
            </h6>
            <h6 className="ml-3 cursor-pointer">
              <a href="https://www.airbnb.co.in/help/article/2908/terms-of-service">
                TERMS
              </a>
            </h6>
          </div>
        </div>
        <div className="collapse-lg navbar-collapse ">
          <div className="d-flex justify-content-around w-100 text-danger">
            <Link to="/search">
              <div className="d-flex flex-column align-items-center  m-0-2 cursor-pointer">
                <i className="fa-solid fa-magnifying-glass"></i>Explore
              </div>
            </Link>
            <Link to="/wishlist">
              <div className="d-flex flex-column align-items-center m-0-2 cursor-pointer">
                <i className="fa-solid fa-heart"></i>Wishlists
              </div>
            </Link>
            <Link to="/login">
              <div className="d-flex flex-column align-items-center  m-0-2 cursor-pointer">
                <i className="fa-solid fa-user"></i>Log In
              </div>
            </Link>
          </div>
        </div>

        <div className="collapse navbar-collapse ">
          <div className="d-flex justify-content-end w-100">
            <h6 className="mr-3">
              ENGLISH<i className="fa-solid fa-globe mr-3 ml-2"></i>
            </h6>
            <h6 className="mr-3">INR</h6>
          </div>
        </div>
      </nav>
    </>
  );
}

export default BottomBar;
