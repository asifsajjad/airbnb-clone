import React from "react";
import { connect } from "react-redux";
import Loader from "./Loader";

function LaptopView({hotelData}) {
  
  return (
    (hotelData === undefined)?
    <Loader />
    :
    <>
      <div className="container-fluid height-control d-none d-lg-flex m-auto p-0">
        <div className="container w-50 h-100 m-0-auto p-0">
          <img
            src={hotelData.images[0]}
            alt="first_image"
            className="image-fit"
          />
        </div>
        <div className="container w-50 h-100 d-flex flex-wrap m-0 p-0">
          <div className="container w-50 h-50 m-auto p-0">
            <img
              src={hotelData.images[1]}
              alt="second_image"
              className="image-fit"
            />
          </div>
          <div className="container w-50 h-50 m-auto p-0">
            <img
              src={hotelData.images[2]}
              alt="third_image"
              className="image-fit"
            />
          </div>
          <div className="container w-50 h-50 m-auto p-0">
            <img
              src={hotelData.images[3]}
              alt="fourth_image"
              className="image-fit"
            />
          </div>
          <div className="container w-50 h-50 m-auto p-0">
            <img
              src={hotelData.images[4]}
              alt="fifth_image"
              className="image-fit"
            />
          </div>
        </div>
      </div>
    </>
  );
}

const mapStateToProps = (state)=>{
  return {
    hotelData:state.apiData.selectedHotel
  }
}

export const ConnectedLaptopView= connect(mapStateToProps,null)(LaptopView);
