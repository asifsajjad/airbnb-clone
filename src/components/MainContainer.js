import React, { Component } from "react";
import { connect } from "react-redux";
import Loader from "./Loader";
import { Link } from "react-router-dom";
// import data from '../data/data.json'

export class MainContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      first: 0,
    };
  }

  generateCards = (inputCardDataArray) => {
    // console.log(inputCardDataArray);
    return inputCardDataArray.map((item) => {
      return (
        <div className="mt-3 col-lg-3 col-md-4 col-sm-11" key={item.id}>
          <div className="card rounded-4 myCards">
            {/* corousel start */}
            <div
              id={`mycarousel${item.id}`}
              className="carousel slide"
              data-ride="carousel"
            >
              <ol className="carousel-indicators">
                <li
                  data-target={`#mycarousel${item.id}`}
                  data-slide-to="0"
                  className="active"
                ></li>
                <li data-target={`#mycarousel${item.id}`} data-slide-to="1"></li>
                <li data-target={`#mycarousel${item.id}`} data-slide-to="2"></li>
                <li data-target={`#mycarousel${item.id}`} data-slide-to="3"></li>
              </ol><Link to={`/${item.id}`}>
              <div className="carousel-inner">
                <div className="carousel-item active">
                  <img
                    className="d-block w-100"
                    src={item.images[1]}
                    alt="First slide"
                  />
                </div>
                <div className="carousel-item">
                  <img
                    className="d-block w-100"
                    src={item.images[2]}
                    alt="Second slide"
                  />
                </div>
                <div className="carousel-item">
                  <img
                    className="d-block w-100"
                    src={item.images[3]}
                    alt="Third slide"
                  />
                </div>
                <div className="carousel-item">
                  <img
                    className="d-block w-100"
                    src={item.images[0]}
                    alt="Fourth slide"
                  />
                </div>
              </div>
              </Link>
              <a
                className="carousel-control-prev"
                href={`#mycarousel${item.id}`}
                role="button"
                data-slide="prev"
              >
                <span
                  className="carousel-control-prev-icon"
                  aria-hidden="true"
                ></span>
                <span className="sr-only">Previous</span>
              </a>
              <a
                className="carousel-control-next"
                href={`#mycarousel${item.id}`}
                role="button"
                data-slide="next"
              >
                <span
                  className="carousel-control-next-icon"
                  aria-hidden="true"
                ></span>
                <span className="sr-only">Next</span>
              </a>
            </div>
            {/* corousel end */}
            <Link to={`/${item.id}`}>
              <div className="card-body cursor-pointer">
                <div className="card-title d-flex justify-content-between">
                  <strong>{item.location}</strong>
                  <div>
                    <small className="d-flex flex-row align-items-center justify-content-end">
                      <i className="fa-solid fa-star mr-1" /> New 
                    </small>
                  </div>
                </div>
                <div className="card-text">
                  Hosted By <strong>{item.hostedBy}</strong>
                </div>
                <div className="card-text text-muted">
                  Availability <strong>yes</strong>
                </div>
                <div className="card-text">
                  Rs <strong>{parseInt(item.price).toLocaleString('en-IN')}</strong> per night
                </div>
              </div>
            </Link>
          </div>
        </div>
      );
    });
  };

  render() {
    const myDataArray = this.props.dataArray;

    return (
      <div className="container d-flex flex-wrap flex-sm-column flex-md-row width-100vw main-container-styles hide-scrollbar">
        {myDataArray === undefined ? (
          <Loader />
        ) : (
          this.generateCards(myDataArray)
        )}
        <hr></hr>
        
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    dataArray: state.apiData.gotData,
  };
};

export const ConnectedMainContainer = connect(
  mapStateToProps,
  null
)(MainContainer);
