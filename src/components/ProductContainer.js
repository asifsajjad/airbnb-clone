import React from "react";
import {ConnectedDescriptionCards} from "./DescriptionCards";
import {ConnectedImageCarousal} from "./ImageCarousal";
import {ConnectedLaptopTitle} from "./LaptopTitle";
import {ConnectedLaptopView} from "./LaptopView";

function ProductContainer() {
  return (
    <div className="container-fluid">
      {/* title */}
      <ConnectedLaptopTitle />
      {/* next image */}
      <ConnectedLaptopView />
      {/* Make a separate container for mobile view */}
      <ConnectedImageCarousal />
      {/* bottom descriptions */}
      <ConnectedDescriptionCards />
    </div>
  );
}

export default ProductContainer;
