import React from "react";
import Loader from "./Loader";
import { connect } from "react-redux";

function ImageCarousal({hotelData}) {
  return (
    (hotelData === undefined)?
    <Loader />
    :
    <><div className="container-fluid col-md-10 col-sm-12 d-lg-none d-md-flex d-sm-flex m-auto p-auto">
       <div
              id={`mycarousel${hotelData.id}`}
              className="carousel slide"
              data-ride="carousel"
            >
              <ol className="carousel-indicators">
                <li
                  data-target={`#mycarousel${hotelData.id}`}
                  data-slide-to="0"
                  className="active"
                ></li>
                <li data-target={`#mycarousel${hotelData.id}`} data-slide-to="1"></li>
                <li data-target={`#mycarousel${hotelData.id}`} data-slide-to="2"></li>
                <li data-target={`#mycarousel${hotelData.id}`} data-slide-to="3"></li>
              </ol>
              <div className="carousel-inner">
                <div className="carousel-item active">
                  <img
                    className="d-block w-100 img-fluid"
                    src={hotelData.images[1]}
                    alt="First slide"
                  />
                </div>
                <div className="carousel-item">
                  <img
                    className="d-block w-100 img-fluid"
                    src={hotelData.images[2]}
                    alt="Second slide"
                  />
                </div>
                <div className="carousel-item">
                  <img
                    className="d-block w-100 img-fluid"
                    src={hotelData.images[3]}
                    alt="Third slide"
                  />
                </div>
                <div className="carousel-item">
                  <img
                    className="d-block w-100 img-fluid"
                    src={hotelData.images[0]}
                    alt="Fourth slide"
                  />
                </div>
              </div>
              
              <a
                className="carousel-control-prev"
                href={`#mycarousel${hotelData.id}`}
                role="button"
                data-slide="prev"
              >
                <span
                  className="carousel-control-prev-icon"
                  aria-hidden="true"
                ></span>
                <span className="sr-only">Previous</span>
              </a>
              <a
                className="carousel-control-next"
                href={`#mycarousel${hotelData.id}`}
                role="button"
                data-slide="next"
              >
                <span
                  className="carousel-control-next-icon"
                  aria-hidden="true"
                ></span>
                <span className="sr-only">Next</span>
              </a>
            </div>
            </div>
    </>
  );
}

const mapStateToProps = (state)=>{
  return {
    hotelData:state.apiData.selectedHotel
  }
}

export const ConnectedImageCarousal = connect(mapStateToProps,null)(ImageCarousal);
