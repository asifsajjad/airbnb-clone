import React from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import Loader from "./Loader";
import * as actions from "../actions";

function DescriptionCards({
  hotelData,
  rooms,
  days,
  adults,
  addToWishlist,
  wishlistData,
}) {
  const generateAmenities = (arrayData) =>
    arrayData.map((item, index) => {
      return <li key={index}> {item}</li>;
    });

  const checkAndAddToWishlist = (id) => {
    if (!wishlistData.some((item) => item.id === id)) {
      addToWishlist(hotelData);
    }
  };
  return hotelData === undefined ? (
    <Loader />
  ) : (
    <>
      <div className="container-fluid d-flex flex-column p-0 mt-3">
        <div className="col-12">
          <h4>{hotelData.title}</h4>
          <h6>{hotelData.location}</h6>
          <hr></hr>
        </div>
        <div className="col-12 d-sm-flex d-md-none">
          <div className="d-flex flex-row justify-content-between">
            <div className="mr-3 d-flex">
              <i className="fa-solid fa-arrow-up-from-bracket"></i>
              <h6 className="ml-2">Share</h6>
            </div>
            <div
              className="mr-3 d-flex cursor-pointer click-div"
              onClick={() => checkAndAddToWishlist(hotelData.id)}
            >
              <i className="fa-solid fa-heart"></i>
              <h6 className="ml-2">Wishlist</h6>
            </div>
          </div>
          <hr></hr>
        </div>

        <div className="col-12">
          <h5>
            Hosted by <strong> {hotelData.hostedBy}</strong>
          </h5>
          <h6>
            <strong>6</strong> Guests. <strong>3</strong> bedrooms,{" "}
            <strong>2</strong> beds. <strong>2</strong> bathroom.{" "}
          </h6>
          <hr></hr>
        </div>
        <div className="col-12">
          <p>{hotelData.description}</p>
        </div>
        <div className="col-12 d-lg-flex">
          <div className="col-lg-6 col-md-12 col-sm-12 p-0">
            <h3>What this place offers</h3>
            <ul>{generateAmenities(hotelData.amenities)}</ul>
          </div>
          <div className="col-lg-3 d-none d-lg-flex border border-danger rounded flex-column justify-content-center align-items-center ml-auto mr-4">
            <h4>
              {" "}
              Price Rs{" "}
              <strong>
                {(hotelData.price * rooms).toLocaleString("en-IN")}
              </strong>{" "}
              night
            </h4>
            <h4>
              <strong>{adults}</strong> Guests and <strong>{days}</strong> days.{" "}
            </h4>

            <Link to="/checkout">
              <button className="btn btn-danger col-12  d-flex align-items-center justify-content-center text-white">
                Check-Out
              </button>
            </Link>
          </div>
        </div>
        <div className="col-12">
          <h4>Reviews</h4>
          <h6>
            <strong> {hotelData.rating} </strong> stars by{" "}
            <strong> {hotelData.count} </strong> People
          </h6>
        </div>
        <div className="col-12 d-flex justify-content-center">
          <button type="button" className="btn btn-outline-danger">
            Contact host
          </button>
        </div>
        <hr></hr>
        <hr></hr>
        <hr></hr>
      </div>
    </>
  );
}

const mapStateToProps = (state) => {
  return {
    hotelData: state.apiData.selectedHotel,
    days: state.apiData.days,
    adults: state.apiData.adults,
    rooms: state.apiData.rooms,
    wishlistData: state.apiData.wishlistData,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    addToWishlist: (hotelObj) => dispatch(actions.addToWishlist(hotelObj)),
  };
};

export const ConnectedDescriptionCards = connect(
  mapStateToProps,
  mapDispatchToProps
)(DescriptionCards);
