import React from "react";

function Category() {
  const myArray = [
    {
      icon:"fa-solid fa-house-chimney",
      text: "Design",
    },
    {
      icon:"fa-solid fa-snowflake",
      text: "Arctic",
    },
    {
      icon:"fa-solid fa-house-user",
      text: "Shared homes",
    },
    {
      icon:"fa-solid fa-igloo",
      text: "OMG!",
    },
    {
      icon:"fa-solid fa-house-flag",
      text: "Camping",
    },
    {
      icon:"fa-solid fa-bed",
      text: "Bed and Breakfast",
    },
    {
      icon:"fa-solid fa-tree",
      text: "Treehouses",
    },
    {
      icon:"fa-solid fa-people-roof",
      text: "A-frames",
    },
    {
      icon:"fa-solid fa-snowflake",
      text: "Caves",
    },
    {
      icon:"fa-solid fa-house-tsunami",
      text: "Surfing",
    },
    {
      icon:"fa-solid fa-chair",
      text: "Countryside",
    },
    {
      icon:"fa-solid fa-house",
      text: "Beaches",
    },
    {
      icon:"fa-solid fa-warehouse",
      text: "Earth homes",
    },
    {
      icon:"fa-solid fa-dungeon",
      text: "Lux",
    },
    {
      icon:"fa-solid fa-house",
      text: "Tropical",
    },
    {
      icon:"fa-solid fa-house-tsunami",
      text: "Amazing pools",
    },
    {
      icon:"fa-solid fa-dungeon",
      text: "Castles",
    },
    {
      icon:"fa-solid fa-house",
      text: "Cabins",
    },
    {
      icon:"fa-solid fa-chair",
      text: "Mansions",
    },
    {
      icon:"fa-solid fa-people-roof",
      text: "Golfing",
    },
    {
      icon:"fa-solid fa-dungeon",
      text: "Historical homes",
    },
  ];

  const createCategories = (inputDataArray) => {
    return inputDataArray.map((item,index) => {
      return (
        <div className=" d-flex flex-column justify-content-center align-items-center mr-4 category-box" key={index}>
          <div>
            <i className={item.icon}></i>
          </div>
          <div className="category-text text-center text-nowrap">{item.text}</div>
        </div>
      );
    });
  };

  return (
    <div className="container d-flex flex-nowrap width-100vw bg-white custom-categories hide-scrollbar">
      {createCategories(myArray)}
    </div>
  );
}

export default Category;
