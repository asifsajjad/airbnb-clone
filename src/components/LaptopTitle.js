import React from "react";
import { connect } from "react-redux";
import Loader from "./Loader";
import * as actions from "../actions";

function LaptopTitle({ hotelData, addToWishlist, wishlistData }) {
  const checkAndAddToWishlist = (id) => {
    if (!wishlistData.some((item) => item.id === id)) {
      addToWishlist(hotelData);
    }
  };

  return hotelData === undefined ? (
    <Loader />
  ) : (
    <>
      <div className="container d-none d-md-flex flex-column justify-content-between p-2">
        <h5>{hotelData.title}</h5>
        <p>
          <u>{hotelData.location}</u>
        </p>
        <div className="d-flex flex-row justify-content-end">
          <div className="mr-3 d-flex">
            <i className="fa-solid fa-arrow-up-from-bracket"></i>
            <h6 className="ml-2">Share</h6>
          </div>
          <div
            className="mr-3 d-flex cursor-pointer click-div"
            onClick={() => checkAndAddToWishlist(hotelData.id)}
          >
            <i className="fa-solid fa-heart"></i>
            <h6 className="ml-2">Wishlist</h6>
          </div>
        </div>
      </div>
    </>
  );
}

const mapStateToProps = (state) => {
  return {
    hotelData: state.apiData.selectedHotel,
    wishlistData: state.apiData.wishlistData,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    addToWishlist: (hotelObj) => dispatch(actions.addToWishlist(hotelObj)),
  };
};

export const ConnectedLaptopTitle = connect(
  mapStateToProps,
  mapDispatchToProps
)(LaptopTitle);
