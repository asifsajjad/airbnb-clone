const initialState = {
  days: 1,
  adults: 1,
  rooms: 1,
  savedUser: "",
  wishlistData: [],
};

const manageAPIData = (state = initialState, action) => {
  switch (action.type) {
    case "FETCH":
      return {
        ...state,
        gotData: action.payload,
      };
    case "FETCHID":
      return {
        ...state,
        selectedHotel: action.payload,
      };
    case "SAVELOGIN":
      return {
        ...state,
        savedUser: action.payload,
      };
    case "SAVEBOOKING":
      return {
        ...state,
        days: action.payload.days,
        adults: action.payload.adults,
        rooms: action.payload.rooms,
      };
    case "SIGNUP":
      return {
        ...state,
        userDetails: action.payload,
        savedUser: action.payload.email,
      };
    case "ORDER":
      return {
        ...state,
        orderData: action.payload,
      };
    case "REDIRECT":
      return {
        ...state,
        checkoutRedirect: action.payload,
      };
    case "ADDWISHLIST":
      return {
        ...state,
        wishlistData: [...state.wishlistData, action.payload],
      };
    case "REMOVEWISHLIST":
      return {
        ...state,
        wishlistData: action.payload,
      };
    case "WISHLISTREDIRECT":
      return {
        ...state,
        wishlistRedirect: action.payload,
      };
    case "TEMPBOOKING":
      return {
        ...state,
        tempBookingData: action.payload,
      };
    default:
      return state;
  }
};

export default manageAPIData;
