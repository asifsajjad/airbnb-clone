import { combineReducers } from "redux";
import manageAPIData from "./manageAPIData";

const myReducer = combineReducers({
  apiData: manageAPIData,
});

export default myReducer;
