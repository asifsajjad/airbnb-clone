import { createStore} from "redux";
import myReducer from "./reducers";
import storage from 'redux-persist/lib/storage';
import {
    persistReducer,
    persistStore,
} from 'redux-persist';

const persistConfig = {
  key: 'root',
  storage,
};

const persistedReducer = persistReducer(persistConfig, myReducer);


const myStore = createStore(
  persistedReducer,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);
let persistor = persistStore(myStore);

const Store= { myStore, persistor }
export default Store;





